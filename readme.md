# The Dr. Sebi Nutritional Guide Revitalized

A void was left in the soul of many people when Dr. Sebi ~was starved in prison~ passed away. Because of this, his dream of healing people were put on hold. To continue this dream, I've created **The Dr. Sebi Nutritional Guide Revitalized!** This guide aims to bring life to the Dr. Sebi Nutritional Guide, improving on many aspects.

## Download
These images are sourced directly from this repo. They will always be the latest version.

<img src="https://gitlab.com/30-silver/nutritional-guide/-/raw/master/scan/nutritional.jpg" />

<img src="https://gitlab.com/30-silver/nutritional-guide/-/raw/master/scan/herbal.jpg">

## Features
1. It's free and open source. The source files are available in this repo. You can modify them if you know how to use [GIMP](https://gimp.org/) (a free image editor). You can actively contribute to its development via pull requests and feedback. It is also licensed under the GNU GPLv3.

2. Completely redesigned from the ground up to have a better, cleaner look that fits on any 18:9 phone screen. 

3. Contains 2x more foods than the original guide, that's like two diets in one!

4. Removes some foods on the original guide that weren't alkaline (such as oranges)

5. Contains a new Alkaline Herbal Guide, which is a massive list of Alkaline herbs and a short summary of what they do.

## Important Links

- [Methodology](https://drsebiscellfood.com/methodology/)
- [License](https://www.gnu.org/licenses/gpl-3.0.en.html)

<hr />
Created by judas<br />
This guide has no association with Dr. Sebi Cell Food or the original guide.